# OJDataDigger

A toy that lets you retrieve data (mostly numbers) from our online judge.

**CAUTION**: Use at your own risk. This tool gets data by somehow spamming the online judge (笑).

## Installation

Make sure that `Google Chrome` and `chromedriver` are installed beforehand. On Arch Linux, this can be done by executing

```bash
yay -S google-chrome
yay -S chromedriver
```

Clone the project, cd into the directory and install npm modules:

```bash
npm install
```

## Usage

Your C++ code has to include specially designed functions and `$SHIFT` serving as the bitwise shift. See `template.cpp` for example.

Fill in the configuration part in `dig_num.js`:

```javascript
// Configuration section
const PROBLEM   = 12345;         // Problem ID
const USERNAME  = 'my_username';
const PASSWORD  = 'my_password';
const BIT_START = 0;             // Starting bit
const BIT_STOP  = 16;            // Ending bit
const MAX_IND   = 1;             // How many test data sets
```

After that you can run `dig_num.js` to start the digging process:

```bash
node dig_num.js
```
