var list = document.getElementsByClassName('danger');

var i;

function bits(str) {
	switch(str)
	{
	case 'A':
		return '11';
	case 'W':
		return '01';
	case 'P':
		return '10';
	case 'T':
		return '00';
	}
}

var MAX_IND = $MAX_IND; // 3
var MAX_ROW = $MAX_ROW; // 8

var strings = [];

for (i = 0; i < MAX_IND; i++)
	strings.push('');

for (i = 0; i < MAX_ROW; i++)
{
	var rgx = /row.>(.)/g;
	var m;
	var j = 0;
	do
	{
		m = rgx.exec(list[i].title);
		if (!m) break;
		strings[j] += bits(m[1]);
		j++;
	} while(m);
}

return JSON.stringify(strings);

