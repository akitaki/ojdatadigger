// Configuration section
const PROBLEM   = 0;
const USERNAME  = '';
const PASSWORD  = '';
const BIT_START = 0;
const BIT_STOP  = 32;
const MAX_IND   = 1;
const HEADLESS  = true;

// URL functions
const submit_url = (problem) => `https://acm.cs.nthu.edu.tw/users/submit/${problem}`;
const login_url  = 'https://acm.cs.nthu.edu.tw/users/login/';
const status_url = (user, problem) => `https://acm.cs.nthu.edu.tw/status/?username=${user}&pid=${problem}&cid=&status=`;

// Require selenium
var webdriver = require('selenium-webdriver');
var By        = webdriver.By;
var until     = webdriver.until;
var chrome    = require('selenium-webdriver/chrome');

// Require code template file
var fs = require('fs');
const code_template = fs.readFileSync('./jose_template.cpp', 'utf8');
const retrieve_template = fs.readFileSync('retrieve_num.js', 'utf8');

/**
 * Get code with desired shift in place.
 * @param  {number} shift The shift.
 * @return {string}       The code, with shift, newlines and tabs replaced.
 */
function code_with_shift(shift)
{
	return code_template.replace(/\\n/g, '\\\\n').replace(/\n/g, '\\n').replace(/\t/g, '\\t').replace('$SHIFT', shift.toString());
}

/**
 * Get the retrieve script with desired max index and max rows.
 * @param  {number} max_ind Max index.
 * @param  {number} max_row Max row.
 * @return {string}         The script string.
 */
function retrieve_with(max_ind, max_row)
{
	return retrieve_template.replace('$MAX_IND', max_ind.toString()).replace('$MAX_ROW', max_row.toString());
}

Promise.prototype.thenWait = function thenWait(time) {
	return this.then(result => new Promise(resolve => setTimeout(resolve, time, result)));
};

// Setup chrome
var chrome_argument = HEADLESS ? '--headless' : '';
var driver = new webdriver.Builder()
	.withCapabilities(webdriver.Capabilities.chrome())
	.setChromeOptions(new chrome.Options().addArguments(chrome_argument))
	.build();

/**
 * Fill in login credentials and click login
 * @return {undefined}
 */
function send_login()
{
	console.log('Logging in...\n');
	driver.findElement(By.id('id_username')).sendKeys(USERNAME);
	driver.findElement(By.id('id_password')).sendKeys(PASSWORD);
	driver.findElement(By.className('btn')).click();
	console.log('Sent login request.\n');
}

/**
 * Open the login page
 * @return {Promise} A promise
 */
function load_login()
{
	return driver.get(login_url);
}

/**
 * Open the submit page
 * @return {Promise} A promise
 */
function load_submit()
{
	return driver.get(submit_url(PROBLEM));
}

/**
 * Open the status page
 * @return {Promise} A promise
 */
function load_status()
{
	return driver.get(status_url(USERNAME, PROBLEM));
}

/**
 * Insert code to the editor text field
 * @param  {string} code
 * @return {Promise}      A promise
 */
function insert_code(code)
{
	return driver.executeScript(`editor.doc.setValue(\`${code}\`)`);
}

/**
 * Select c++17 in the dropdown
 * @return {Promise} A promise
 */
function select_cpp17()
{
	return driver.executeScript('document.getElementsByTagName(\'option\')[4].selected = true');
}

/**
 * Press the submit button
 * @return {Promise} A promise
 */
function send_submission()
{
	return driver.findElement(By.className('btn-primary')).click();
}

/**
 * Open submit page, insert code with correct shift, and submit.
 * @param  {number} shift Shift, usually between 0 and 32.
 * @param  {number} end   The highest bit to get.
 *                        Used as the ending condition of the recursion.
 * @return {Promise}      A promise
 */
function dig_num(shift, end)
{
	return load_submit().then(() => {
		return driver.wait(until.titleIs('Submit'));
	}).then(() => {
		return driver.getTitle();
	}).then(title => {
		console.log(`Page title is now ${title}`);
		return driver.wait(until.elementLocated(By.xpath('//script[@src=\'/static/js/editorSettings.js\']')));
	}).then(() => {
		console.log(`Inserting code (shift=${shift}) to the editor...`);
		return insert_code(code_with_shift(shift));
	}).then(() => {
		console.log('Code insertion done.');
		return driver.wait(until.elementLocated(By.xpath('//option[@value=\'CPP17\']')));
	}).then(() => {
		return select_cpp17();
	}).then(() => {
		return send_submission();
	}).then(() => {
		return driver.wait(until.titleIs('Status'));
	}).then(() => {
		if (shift + 2 < end)
			return dig_num(shift + 2, end);
	});
}

/**
 * Wait until all queries are judged. Recursive.
 * @return {undefined}
 */
function wait_til_judged()
{
	return load_status().thenWait(3000).then(() => {
		return driver.executeScript('var list = document.getElementsByTagName("td"); for (var ele of list) { if (ele.innerHTML.includes("Judging") || ele.innerHTML.includes("Being Judged")) return false; }');
	}).then(result => {
		if (result == false)
		{
			console.log('Some queries are not judged yet.\nWaiting for another 3 sec...');
			return wait_til_judged();
		}
		else
		{
			console.log('All queries are judged.');
		}
	});
}

/**
 * Get and print the resulting numbers in both binary and decimal form.
 * @return {Promise} A promise (currently useless).
 */
function retrieve_binary()
{
	return driver.executeScript(retrieve_with(MAX_IND, (BIT_STOP - BIT_START) / 2)).then(result => {
		var bin_list = JSON.parse(result);
		console.log('\nThe resulting binary string:');
		console.log(bin_list, '\n');

		for (var str of bin_list)
			console.log(parseInt(str));
	});
}

load_login().then(() => {
	send_login();
	return driver.wait(until.titleIs('Index'));
}).then(() => {
	return driver.getTitle();
}).then(title => {
	console.log(`Page title is now ${title}`);
	return dig_num(BIT_START, BIT_STOP);
}).then(() => {
	console.log('Submission done!');
	return load_status();
}).then(() => {
	return wait_til_judged();
}).then(() => {
	return retrieve_binary();
});
